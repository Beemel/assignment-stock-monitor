// declaring some consts and requirement stuff here
const {Console} = require('console');
const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const {Server} = require("socket.io");
const io = new Server(server);
const {message} = require('statuses');
const path = require('path');
const fs = require("fs")

// use static folder (in this case only for CSS files.) Question: what more files/data could/should/usually in folder
app.use(express.static(path.join(__dirname, 'public', 'static')))

// create the initial stocklist
stockInfo = {
    stockOne: "ABC Traders",
    priceOne: 44.51,
    stockTwo: "Trendy FinTech",
    priceTwo: 23.99,
    stockThree: "SuperCorp",
    priceThree: 126.91
}
let updatedPriceOne =stockInfo.priceOne
let updatedPriceTwo =stockInfo.priceTwo
let updatedPriceThree =stockInfo.priceThree


// enable JSON support
app.use(express.json());


// when server root (URL in browser: localhost:3000 or 127.0.0.1:3000) is accessed/req serve/res index.html to browser.
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'index.html'))
})

// when /current is requested serve updated stock info to browser
app.get('/current', (req, res) => {
    currentStockInfo = {
        stockOne: "ABC Traders",
        priceOne: updatedPriceOne,
        stockTwo: "Trendy FinTech",
        priceTwo: updatedPriceTwo,
        stockThree: "SuperCorp",
        priceThree: updatedPriceThree
    }
    res.send(currentStockInfo)
})

// patch new values. (This require that you send the data from Postman?!?)
// i get a feeling that i am missing something here. But it works fine req/res with Postman. 

app.patch('/update', (req, res) => {
    const {data} = req.body

    // compare the patch info with what is current and respond accordingly, also send the info both out on IO as well as a response to postman. 
        // I liked to have done this in a better and more elegant way...
        // But in frustration i reverted back to the things that i understand best right now, and that is the logic of "if"...


    // compare stock name from the Patch to see if it correspond to any of the existing stocks...
    if (data.stockName == stockInfo.stockOne || data.stockName == stockInfo.stockTwo || data.stockName == stockInfo.stockThree) {
    //...if stock name is found in monitored list. Compare current price with existing one and reply accordingly, and then emit the info.

        // if thingy for ABC Traders
        if (data.stockName == stockInfo.stockOne) {
            if (data.price == stockInfo.priceOne) {
                console.log('price unchanged')
                io.emit('sendMessage', data)
                res.send('Unchanged value, price remains at: '+ stockInfo.priceOne )
                return updatedPriceOne = data.price
            } else if (data.price < stockInfo.priceOne) {
                console.log('price decreased')
                io.emit('sendMessage', data)
                res.send('price decreased from: '+ stockInfo.priceOne + ' to: ' + data.price )
                return updatedPriceOne = data.price
            } else if (data.price > stockInfo.priceOne) {
                console.log('price increased')
                io.emit('sendMessage', data)
                res.send('price increased from: '+ stockInfo.priceOne + ' to: ' + data.price )
                return updatedPriceOne = data.price
            }
        }
        // if thingy for Trendy FinTech
        if (data.stockName == stockInfo.stockTwo) {
            if (data.price == stockInfo.priceTwo) {
                console.log('price unchanged')
                io.emit('sendMessage', data)
                res.send('Unchanged value, price remains at: '+ stockInfo.priceTwo )
                return updatedPriceTwo = data.price
            } else if (data.price < stockInfo.priceTwo) {
                console.log('price decreased')
                io.emit('sendMessage', data)
                res.send('Value decreased from: '+ stockInfo.priceTwo + ' to: ' + data.price )
                return updatedPriceTwo = data.price
            } else if (data.price > stockInfo.priceTwo) {
                console.log('price increased')
                io.emit('sendMessage', data)
                res.send('Value increased from: '+ stockInfo.priceTwo + ' to: ' + data.price )
                return updatedPriceTwo = data.price
            }
        }
        // if thingy for SuperCorp
        if (data.stockName == stockInfo.stockThree) {
            if (data.price == stockInfo.priceThree) {
                console.log('price unchanged')
                io.emit('sendMessage', data)
                res.send('Unchanged value, price remains at: '+ stockInfo.priceThree )
                return updatedPriceThree = data.price            
            } else if (data.price < stockInfo.priceThree) {
                console.log('price decreased')
                io.emit('sendMessage', data)
                res.send('Value decreased from: '+ stockInfo.priceThree + ' to: ' + data.price )
                return updatedPriceThree = data.price
            } else if (data.price > stockInfo.priceThree) {
                console.log('price increased')
                console.log (updatedPriceThree)
                io.emit('sendMessage', data)
                res.send('Value increased from: '+ stockInfo.priceThree + ' to: ' + data.price )
                return updatedPriceThree = data.price
            }
        }
    } 
    // if no stock is found log error and reply to postman dont emit any update.
    else {
        console.log('Stock not found')
        res.send('Stock not found')
    }
})
//Starting server and send message to console as verfication 
server.listen(3000, () => {
    console.log('Server has started')
})